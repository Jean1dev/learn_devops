const axios = require('axios')
const express = require('express')
const app = express()
 
app.get('/', async function (req, res) {
  const ret = await axios.get('http://app2:3001/')
  return res.json(ret.data)
})
 
app.listen(process.env.PORT || 3000)